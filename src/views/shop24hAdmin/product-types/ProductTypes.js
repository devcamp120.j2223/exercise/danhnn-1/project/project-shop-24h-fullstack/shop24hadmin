import React, { useState, useEffect } from 'react'

import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CButton,
  CButtonGroup,
} from '@coreui/react'
import {
  Grid,
  InputLabel,
  Select,
  MenuItem,
  ButtonGroup,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
} from '@mui/material'

//Style
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  minWidth: 360,
  bgcolor: 'White',
  border: '2px solid #000',
  boxShadow: 24,
  fontWeight: 'bold',
}

import ModalAddNew from './modal/ModalAddNew'
import ModalDelete from './modal/ModalDelete'
import ModalEdit from './modal/ModalEdit'
const ProductTypes = () => {
  const [productTypes, setProductTypes] = useState([])
  const [typeName, setTypeName] = useState('')

  //PANIGATION
  //Limit: số lượng bản ghi trên 1 trang
  const [limit, setLimit] = useState(10)
  //số trang: tổng số lượng sản phẩm / limit - Số lớn hơn gần nhất
  const [noPage, setNoPage] = useState(0)
  //Trang hiện tại
  const [page, setPage] = useState(1)
  //Load trang
  const [varRefeshPage, setVarRefeshPage] = useState(0)
  //PANIGATION
  const handleChangeLimit = (event) => {
    setLimit(event.target.value)
  }
  //MODAL
  const [openModalAdd, setOpenModalAdd] = useState(false)
  const [openModalEdit, setOpenModalEdit] = useState(false)
  const [openModalDelete, setOpenModalDelete] = useState(false)
  //Đóng Modal
  const handleClose = () => setOpenModalAdd(false)
  const handleCloseEdit = () => setOpenModalEdit(false)
  const handleCloseDelete = () => setOpenModalDelete(false)
  //Modal Add New
  const onBtnAddOrderClick = () => {
    console.log('Nút thêm được click')
    setOpenModalAdd(true)
  }
  //Modal Delete
  //ID
  const [idDelete, setIdDelete] = useState({ id: 0 })
  const [nameDelete, setNameDelete] = useState('')
  //Modal Edit
  const [dataEdit, setDataEdit] = useState({})

  const onBtnDeleteClick = (data) => {
    setOpenModalDelete(true)
    setIdDelete(data._id)
    setNameDelete(data.name)
  }
  const onBtnEditClick = (data) => {
    setOpenModalEdit(true)
    setDataEdit(data)
  }

  const fetchAPI = async (url, body) => {
    const response = await fetch(url, body)
    const data = await response.json()
    return data
  }

  useEffect(() => {
    if (typeName == '') {
      fetchAPI('http://localhost:8000/producttypes')
        .then((data) => {
          setNoPage(Math.ceil(data.data.length / limit))

          setProductTypes(data.data.slice((page - 1) * limit, page * limit))
        })
        .catch((error) => {
          console.error(error.message)
        })
    } else {
      fetchAPI(`http://localhost:8000/producttypes?name=${typeName}`)
        .then((data) => {
          setNoPage(Math.ceil(data.data.length / limit))

          setProductTypes(data.data.slice((page - 1) * limit, page * limit))
        })
        .catch((error) => {
          console.error(error.message)
        })
    }
  }, [typeName, page, limit, varRefeshPage])

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Product Types</strong>
          </CCardHeader>
          <CCardBody>
            <h3 className="text-medium-emphasis" align="center">
              Danh sách loại sản phẩm
            </h3>
            <Grid container mt={1} mb={1}>
              <Grid item xs="4">
                <Grid item xs={12}>
                  <CButton onClick={onBtnAddOrderClick} className="btn btn-success text-white">
                    Add new
                  </CButton>
                </Grid>
              </Grid>
              <Grid item xs="8">
                <Grid container justifyContent={'flex-end'}>
                  <Grid item marginY={'auto'} mr={1}>
                    <InputLabel>Show products</InputLabel>
                  </Grid>
                  <Grid item>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={limit}
                      size="small"
                      onChange={handleChangeLimit}
                    >
                      <MenuItem value={5}>5</MenuItem>
                      <MenuItem value={10}>10</MenuItem>
                      <MenuItem value={25}>25</MenuItem>
                      <MenuItem value={50}>50</MenuItem>
                    </Select>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>

            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell align="center">STT</TableCell>
                    <TableCell align="center">Name</TableCell>
                    <TableCell align="center">Description</TableCell>
                    <TableCell align="center">Action</TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {productTypes.map((productType, index) => {
                    return (
                      <TableRow key={index}>
                        <TableCell scope="row">{index + 1}</TableCell>
                        <TableCell>{productType.name}</TableCell>
                        <TableCell>{productType.description}</TableCell>
                        <TableCell>
                          <ButtonGroup>
                            <Button
                              onClick={() => {
                                onBtnEditClick(productType)
                              }}
                              variant="contained"
                              color="success"
                            >
                              EDIT
                            </Button>
                            <Button
                              onClick={() => {
                                onBtnDeleteClick(productType)
                              }}
                              variant="contained"
                              color="error"
                            >
                              DELETE
                            </Button>
                          </ButtonGroup>
                        </TableCell>
                      </TableRow>
                    )
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </CCardBody>
          <ModalAddNew
            varRefeshPage={varRefeshPage}
            setOpenModalAdd={setOpenModalAdd}
            openModalAdd={openModalAdd}
            handleClose={handleClose}
            style={style}
            fetchAPI={fetchAPI}
            setVarRefeshPage={setVarRefeshPage}
          />
          <ModalDelete
            varRefeshPage={varRefeshPage}
            setVarRefeshPage={setVarRefeshPage}
            style={style}
            openModalDelete={openModalDelete}
            idDelete={idDelete}
            nameDelete={nameDelete}
            handleCloseDelete={handleCloseDelete}
          />
          <ModalEdit
            varRefeshPage={varRefeshPage}
            dataEdit={dataEdit}
            setDataEdit={setDataEdit}
            openModalEdit={openModalEdit}
            handleCloseEdit={handleCloseEdit}
            style={style}
            fetchAPI={fetchAPI}
            setVarRefeshPage={setVarRefeshPage}
          />
        </CCard>
      </CCol>
    </CRow>
  )
}

export default ProductTypes
