import { Grid, Alert, Box, Button, Modal, Snackbar, Typography, ButtonGroup } from '@mui/material'
import { useState, useEffect } from 'react'
import { CFormInput } from '@coreui/react'
function ModalAddNew({
  openModalAdd,
  setOpenModalAdd,
  handleClose,
  style,
  fetchAPI,
  setVarRefeshPage,
  varRefeshPage,
}) {
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')

  //Alert
  const [openAlert, setOpenAlert] = useState(false)
  const [statusModal, setStatusModal] = useState('error')
  const [noidungAlertValid, setNoidungAlertValid] = useState('')

  //BTN ADD NEW
  const onBtnInsertClick = () => {
    if (valiDate()) {
      const body = {
        method: 'POST',
        body: JSON.stringify({
          name: name,
          description: description,
        }),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      }
      fetchAPI('http://localhost:8000/producttypes', body)
        .then((data) => {
          setOpenAlert(true)
          setStatusModal('success')
          setNoidungAlertValid('Dữ liệu thêm thành công!')
          setOpenModalAdd(false)
          setVarRefeshPage(varRefeshPage + 1)
          console.log(data)
          // window.location.reload();
        })
        .catch((error) => {
          setOpenAlert(true)
          setStatusModal('error')
          setNoidungAlertValid('Dữ liệu thêm thất bại!')
          console.log(error.message)
        })
    }
  }

  //Validate
  const valiDate = () => {
    if (name == '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa đặt tên cho loại sản phẩm')
      return false
    }
    if (description === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa có mô tả loại sản phẩm')
      return false
    }
    return true
  }

  //Đóng Alert
  const handelCloseAlert = () => {
    setOpenAlert(false)
  }

  //Đóng Modal
  const onBtnCancelClick = () => {
    handleClose()
  }

  return (
    <>
      <Modal
        open={openModalAdd}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box p={2} borderRadius={3} sx={style}>
          <Typography sx={{ fontSize: { xs: '4vw', md: '2vw' } }} align="center">
            <strong>Thêm Loại Sản Phẩm</strong>
          </Typography>

          <Grid container pt={2}>
            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  size="small"
                  value={name}
                  placeholder="name"
                  onChange={(event) => setName(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  value={description}
                  size="small"
                  placeholder="description"
                  onChange={(event) => setDescription(event.target.value)}
                />
              </Grid>
            </Grid>
          </Grid>

          <Grid className="mt-4 text-center">
            <Grid justifyContent={'center'} item sm={12}>
              <ButtonGroup>
                <Button onClick={onBtnInsertClick} className="bg-success text-white">
                  Thêm Loại Sản Phẩm
                </Button>
                <Button onClick={onBtnCancelClick} className="bg-secondary text-white">
                  Hủy Bỏ
                </Button>
              </ButtonGroup>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handelCloseAlert}>
        <Alert onClose={handelCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default ModalAddNew
