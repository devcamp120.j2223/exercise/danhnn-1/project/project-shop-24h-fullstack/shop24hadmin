import { Grid, Alert, Box, Button, Modal, Snackbar, Typography, ButtonGroup } from '@mui/material'
import { useState, useEffect } from 'react'
import { CFormInput } from '@coreui/react'
function ModalEdit({
  varRefeshPage,
  dataEdit,
  openModalEdit,
  handleCloseEdit,
  style,
  setDataEdit,
  fetchAPI,
  setVarRefeshPage,
}) {
  //Alert
  const [openAlert, setOpenAlert] = useState(false)
  const [statusModal, setStatusModal] = useState('error')
  const [noidungAlertValid, setNoidungAlertValid] = useState('')

  //Đóng Alert
  const handelCloseAlert = () => {
    setOpenAlert(false)
  }
  //Đóng Modal
  const onBtnCancelClick = () => {
    handleCloseEdit()
  }
  const setDataTypeName = (data) => {
    setDataEdit({ name: data, description: dataEdit.description, _id: dataEdit._id })
  }
  const setDataTypeDesription = (data) => {
    setDataEdit({ name: dataEdit.name, description: data, _id: dataEdit._id })
  }
  const validateData = () => {
    if (dataEdit.name.trim().length <= 1) {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa đặt tên cho loại sản phẩm')
      return false
    }
    if (dataEdit.description.trim().length <= 1) {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa có mô tả loại sản phẩm')
      return false
    }
    return true
  }
  const callApiUpdateDataType = async () => {
    console.log(dataEdit)
    const body = {
      method: 'PUT',
      body: JSON.stringify({
        name: dataEdit.name,
        description: dataEdit.description,
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    fetchAPI(`http://localhost:8000/producttypes/${dataEdit._id}`, body)
      .then((data) => {
        setOpenAlert(true)
        setStatusModal('success')
        setNoidungAlertValid('Dữ liệu update thành công!')
        handleCloseEdit()
        setVarRefeshPage(varRefeshPage + 1)
        console.log(data)
      })
      .catch((error) => {
        setOpenAlert(true)
        setStatusModal('error')
        setNoidungAlertValid('Dữ liệu thêm thất bại!')
        console.log(error.message)
      })
  }
  const onBtnUpdateClick = async () => {
    let validate = validateData()
    if (validate === true) await callApiUpdateDataType()
  }
  useEffect(() => {
    // console.log(dataEdit)
  })

  return (
    <>
      <Modal open={openModalEdit} onClose={handleCloseEdit}>
        <Box p={2} borderRadius={3} sx={style}>
          <Typography sx={{ fontSize: { xs: '4vw', md: '2vw' } }} align="center">
            <strong>Sửa Loại Sản Phẩm</strong>
          </Typography>

          <Grid container pt={2}>
            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  size="small"
                  value={dataEdit.name}
                  placeholder="name"
                  onChange={(event) => setDataTypeName(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  value={dataEdit.description}
                  size="small"
                  placeholder="description"
                  onChange={(event) => setDataTypeDesription(event.target.value)}
                />
              </Grid>
            </Grid>
          </Grid>

          <Grid container className="mt-4 text-center">
            <Grid justifyContent={'center'} item sm={12}>
              <ButtonGroup>
                <Button onClick={onBtnUpdateClick} className="bg-success text-white">
                  Sửa Loại Sản Phẩm
                </Button>
                <Button onClick={onBtnCancelClick} className="bg-secondary text-white">
                  Hủy Bỏ
                </Button>
              </ButtonGroup>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handelCloseAlert}>
        <Alert onClose={handelCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default ModalEdit
