import React, { useState, useEffect } from 'react'

import { CCard, CCardBody, CCardHeader, CCol, CRow, CButton } from '@coreui/react'
import {
  Grid,
  Pagination,
  InputLabel,
  Select,
  MenuItem,
  Button,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from '@mui/material'

import ModalAddNew from './modal/ModalAddNew'
import ModalDelete from './modal/ModalDelete'
import ModalEdit from './modal/ModalEdit'

//Style
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '360px',
  bgcolor: 'white',
  border: '2px solid #000',
  boxShadow: 24,
  borderRadius: '15px',
  fontWeight: 'bold',
}

const Products = () => {
  const [rowProducts, setRowProducts] = useState([])

  const [nameProduct, setNameProduct] = useState('')

  //PANIGATION
  //Limit: số lượng bản ghi trên 1 trang
  const [limit, setLimit] = useState(10)
  //số trang: tổng số lượng sản phẩm / limit - Số lớn hơn gần nhất
  const [noPage, setNoPage] = useState(0)
  //Trang hiện tại
  const [page, setPage] = useState(1)
  //Load trang
  const [varRefeshPage, setVarRefeshPage] = useState(0)

  //PANIGATION
  const handleChangeLimit = (event) => {
    setLimit(event.target.value)
  }
  const onChangePagination = (event, value) => {
    setPage(value)
  }

  //MODAL
  const [openModalAdd, setOpenModalAdd] = useState(false)
  const [openModalEdit, setOpenModalEdit] = useState(false)
  const [openModalDelete, setOpenModalDelete] = useState(false)

  //Đóng Modal
  const handleClose = () => setOpenModalAdd(false)
  const handleCloseEdit = () => setOpenModalEdit(false)
  const handleCloseDelete = () => setOpenModalDelete(false)

  //Modal Add New
  const onBtnAddOrderClick = () => {
    console.log('Nút thêm được click')
    setOpenModalAdd(true)
  }

  //Modal Delete
  //ID
  const [idDelete, setIdDelete] = useState({ id: 0 })
  const [nameDelete, setNameDelete] = useState('')
  //Modal update
  const [dataEdit, setDataEdit] = useState({})

  const onBtnDeleteClick = (data) => {
    console.log('Nút xóa được click')
    console.log('ID: ' + data._id)
    setOpenModalDelete(true)
    setIdDelete(data._id)
    setNameDelete(data.name)
  }
  const onBtnEditClick = (data) => {
    setOpenModalEdit(true)
    setDataEdit(data)
  }

  //LOAD  API
  const fetchAPI = async (url, body) => {
    const response = await fetch(url, body)
    const data = await response.json()
    return data
  }

  useEffect(() => {
    if (nameProduct == '') {
      fetchAPI('http://localhost:8000/products')
        .then((data) => {
          setNoPage(Math.ceil(data.data.length / limit))

          setRowProducts(data.data.slice((page - 1) * limit, page * limit))
          console.log(data)
        })
        .catch((error) => {
          console.error(error.message)
        })
    } else {
      fetchAPI(`http://localhost:8000/products?name=${nameProduct}`)
        .then((data) => {
          setNoPage(Math.ceil(data.data.length / limit))

          setRowProducts(data.data.slice((page - 1) * limit, page * limit))
          console.log(data)
        })
        .catch((error) => {
          console.error(error.message)
        })
    }
  }, [nameProduct, page, limit, varRefeshPage])

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Products</strong>
          </CCardHeader>
          <CCardBody>
            <h3 className="text-medium-emphasis" align="center">
              Danh sách sản phẩm
            </h3>

            <Grid container mt={1} mb={1}>
              <Grid item xs="4">
                <Grid item xs={12}>
                  <CButton onClick={onBtnAddOrderClick} className="btn btn-success text-white">
                    Add new
                  </CButton>
                </Grid>
              </Grid>
              <Grid item xs="8">
                <Grid container justifyContent={'flex-end'}>
                  <Grid item marginY={'auto'} mr={1}>
                    <InputLabel>Show products</InputLabel>
                  </Grid>
                  <Grid item>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={limit}
                      size="small"
                      onChange={handleChangeLimit}
                    >
                      <MenuItem value={5}>5</MenuItem>
                      <MenuItem value={10}>10</MenuItem>
                      <MenuItem value={25}>25</MenuItem>
                      <MenuItem value={50}>50</MenuItem>
                    </Select>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>

            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    {/* <TableCell align="center">STT</TableCell> */}
                    <TableCell align="center">IMG</TableCell>
                    <TableCell align="center">Name</TableCell>
                    <TableCell align="center">Buy Price</TableCell>
                    <TableCell align="center">Promotion Price</TableCell>
                    <TableCell align="center">Amount</TableCell>
                    <TableCell align="center">Action</TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {rowProducts.map((product, index) => {
                    return (
                      <TableRow key={index}>
                        {/* <TableCell scope="row">{index + 1}</TableCell> */}
                        <TableCell>
                          <img src={product.imageUrl} width="50vW" />
                        </TableCell>
                        <TableCell>{product.name}</TableCell>
                        <TableCell>{product.buyPrice.toLocaleString()}</TableCell>
                        <TableCell>{product.promotionPrice.toLocaleString()}</TableCell>
                        <TableCell>{product.amount}</TableCell>
                        <TableCell>
                          <Button
                            onClick={() => {
                              onBtnEditClick(product)
                            }}
                            style={{ color: 'white' }}
                            className="bg-success w-50"
                          >
                            edit
                          </Button>
                          <Button
                            style={{ color: 'white' }}
                            className="bg-danger w-50"
                            onClick={() => {
                              onBtnDeleteClick(product)
                            }}
                          >
                            delete
                          </Button>
                        </TableCell>
                      </TableRow>
                    )
                  })}
                </TableBody>
              </Table>
            </TableContainer>
            <Grid container mt={3} mb={2} justifyContent="flex-end">
              <Grid item>
                <Pagination
                  count={noPage}
                  color="primary"
                  defaultPage={1}
                  onChange={onChangePagination}
                />
              </Grid>
            </Grid>
          </CCardBody>
          <ModalAddNew
            varRefeshPage={varRefeshPage}
            setOpenModalAdd={setOpenModalAdd}
            openModalAdd={openModalAdd}
            handleClose={handleClose}
            style={style}
            fetchAPI={fetchAPI}
            setVarRefeshPage={setVarRefeshPage}
          />
          <ModalDelete
            varRefeshPage={varRefeshPage}
            setVarRefeshPage={setVarRefeshPage}
            style={style}
            openModalDelete={openModalDelete}
            idDelete={idDelete}
            nameDelete={nameDelete}
            handleCloseDelete={handleCloseDelete}
          />
          <ModalEdit
            varRefeshPage={varRefeshPage}
            setOpenModalEdit={setOpenModalEdit}
            openModalEdit={openModalEdit}
            handleCloseEdit={handleCloseEdit}
            style={style}
            fetchAPI={fetchAPI}
            setVarRefeshPage={setVarRefeshPage}
            dataEdit={dataEdit}
            setDataEdit={setDataEdit}
          />
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Products
