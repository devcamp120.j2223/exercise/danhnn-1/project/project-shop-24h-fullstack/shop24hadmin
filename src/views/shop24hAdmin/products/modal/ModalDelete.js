import { Alert, Button, Modal, Snackbar, Typography, Box, Grid, ButtonGroup } from '@mui/material'
import { useEffect, useState } from 'react'

function ModalDelete({
  openModalDelete,
  idDelete,
  nameDelete,
  handleCloseDelete,
  style,
  setVarRefeshPage,
  varRefeshPage,
}) {
  const [openAlert, setOpenAlert] = useState(false)
  const [statusModalDelete, setStatusModalEdit] = useState('error')
  const [noidungAlert, setNoidungAlert] = useState('')

  const handleCloseAlert = () => {
    setOpenAlert(false)
  }

  const onBtnCancelClick = () => {
    handleCloseDelete()
  }

  const onBtnConfirmDeleteClick = () => {
    fetch('http://localhost:8000/products/' + idDelete, { method: 'DELETE' })
      .then((data) => {
        console.log('Delete successful')
        setOpenAlert(true)
        setStatusModalEdit('success')
        setNoidungAlert('Xóa Sản Phẩm ' + idDelete + ' thành công!')
        setVarRefeshPage(varRefeshPage + 1)
        handleCloseDelete()
      })
      .catch((error) => {
        console.error('There was an error!', error)
        setOpenAlert(true)
        setStatusModalEdit('error')
        setNoidungAlert('Xóa Sản Phẩm ' + { idDelete } + ' thất bại!')
        handleCloseDelete()
      })
  }

  return (
    <>
      <Modal open={openModalDelete} onClose={handleCloseDelete}>
        <Box p={2} borderRadius={3} sx={style}>
          <Typography sx={{ fontSize: { xs: '4vw', md: '2vw' } }} align="center">
            <b>Delete Product!</b>
          </Typography>
          <Grid container className="mt-2">
            <Grid item xs={12} align="center">
              <h4>
                Bạn có thật sự muốn xóa sản phẩm:
                <h4 style={{ color: 'red', marginTop: '20px' }}>{nameDelete} ? </h4>
              </h4>
            </Grid>
          </Grid>
          <Grid className="mt-4 text-center">
            <Grid justifyContent={'center'} item sm="12">
              <ButtonGroup>
                <Button onClick={onBtnConfirmDeleteClick} className="bg-danger w-100 text-white">
                  Xác nhận
                </Button>
                <Button onClick={onBtnCancelClick} className="bg-success w-100 text-white">
                  Hủy Bỏ
                </Button>
              </ButtonGroup>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={statusModalDelete} sx={{ width: '100%' }}>
          {noidungAlert}
        </Alert>
      </Snackbar>
    </>
  )
}

export default ModalDelete
