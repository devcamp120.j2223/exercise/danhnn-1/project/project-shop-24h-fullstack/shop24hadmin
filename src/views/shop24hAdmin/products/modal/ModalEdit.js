import {
  Container,
  Grid,
  Alert,
  Box,
  Button,
  FormControl,
  Modal,
  Select,
  Snackbar,
  Typography,
  MenuItem,
  ButtonGroup,
} from '@mui/material'
import { useState, useEffect } from 'react'
import { CFormInput } from '@coreui/react'

function ModalEdit({
  varRefeshPage,
  setOpenModalEdit,
  openModalEdit,
  handleCloseEdit,
  style,
  fetchAPI,
  setVarRefeshPage,
  dataEdit,
  setDataEdit,
}) {
  //Alert
  const [openAlert, setOpenAlert] = useState(false)
  const [statusModal, setStatusModal] = useState('error')
  const [noidungAlertValid, setNoidungAlertValid] = useState('')

  //Select Type
  const [productTypes, setProductTypes] = useState([])

  useEffect(() => {
    fetchAPI('http://localhost:8000/producttypes')
      .then((data) => {
        setProductTypes(data.data)
        console.log(data.data)
      })
      .catch((error) => {
        console.error(error.message)
      })
  }, [])
  //chỉnh sửa
  const onBtnUpdatetClick = () => {
    if (valiDate()) {
      const body = {
        method: 'PUT',
        body: JSON.stringify({
          name: dataEdit.name,
          description: dataEdit.description,
          type: dataEdit.type,
          imageUrl: dataEdit.imageUrl,
          buyPrice: new Number(dataEdit.buyPrice),
          promotionPrice: new Number(dataEdit.promotionPrice),
          amount: new Number(dataEdit.amount),
        }),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      }
      fetchAPI(`http://localhost:8000/products/${dataEdit._id}`, body)
        .then((data) => {
          setOpenAlert(true)
          setStatusModal('success')
          setNoidungAlertValid('Dữ liệu thêm thành công!')
          setOpenModalEdit(false)
          setVarRefeshPage(varRefeshPage + 1)
          console.log(data)
        })
        .catch((error) => {
          setOpenAlert(true)
          setStatusModal('error')
          setNoidungAlertValid('Dữ liệu thêm thất bại!')
          console.log(error.message)
        })
    }
  }
  //Validate
  const valiDate = () => {
    if (dataEdit.name == '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa đặt tên cho sản phẩm')
      return false
    }
    if (dataEdit.description === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa có mô tả cho sản phẩm')
      return false
    }
    if (dataEdit.type === 'NOT') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa chọn loại sản phẩm')
      return false
    }
    if (dataEdit.imageUrl.indexOf('.') === -1) {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('ImageUrl không hợp lệ')
      return false
    }
    if (dataEdit.buyPrice === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa có giá sản phẩm')
      return false
    }
    if (dataEdit.promotionPrice === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa có giá mới sản phẩm')
      return false
    }
    if (dataEdit.amount === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa có số lượng sản phẩm')
      return false
    }
    return true
  }
  //Đóng Alert
  const handelCloseAlert = () => {
    setOpenAlert(false)
  }
  //Đóng Modal
  const onBtnCancelClick = () => {
    setOpenModalEdit()
  }
  const setName = (data) => {
    setDataEdit({ ...dataEdit, name: data })
  }
  const setDescription = (data) => {
    setDataEdit({ ...dataEdit, description: data })
  }
  const setType = (data) => {
    setDataEdit({ ...dataEdit, type: data })
  }
  const setImageUrl = (data) => {
    setDataEdit({ ...dataEdit, imageUrl: data })
  }
  const setBuyPrice = (data) => {
    setDataEdit({ ...dataEdit, buyPrice: data })
  }
  const setPromotionPrice = (data) => {
    setDataEdit({ ...dataEdit, promotionPrice: data })
  }
  const setAmount = (data) => {
    setDataEdit({ ...dataEdit, amount: data })
  }

  return (
    <>
      <Modal open={openModalEdit} onClose={handleCloseEdit}>
        <Box p={2} sx={style}>
          <Typography sx={{ fontSize: { xs: '4vw', md: '2vw' } }} align="center">
            <strong>Sửa Sản Phẩm</strong>
          </Typography>

          <Grid container pt={2}>
            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  placeholder="Name"
                  className="bg-white"
                  size="small"
                  value={dataEdit.name}
                  onChange={(event) => setName(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  value={dataEdit.description}
                  placeholder="Description"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setDescription(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <FormControl fullWidth>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={dataEdit.type}
                    placeholder="Age"
                    onChange={(event) => setType(event.target.value)}
                  >
                    <MenuItem value={'NOT'}>Type</MenuItem>
                    {productTypes.map((type, index) => {
                      return (
                        <MenuItem key={index} value={type._id}>
                          {type.name}
                        </MenuItem>
                      )
                    })}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  value={dataEdit.imageUrl}
                  placeholder="ImageUrl"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setImageUrl(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  type="number"
                  fullWidth
                  value={dataEdit.buyPrice}
                  placeholder="BuyPrice"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setBuyPrice(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  type="number"
                  fullWidth
                  value={dataEdit.promotionPrice}
                  placeholder="PromotionPrice"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setPromotionPrice(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  type="number"
                  fullWidth
                  value={dataEdit.amount}
                  placeholder="Amount"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setAmount(event.target.value)}
                />
              </Grid>
            </Grid>
          </Grid>

          <Grid className="mt-4 text-center">
            <Grid justifyContent={'center'} item sm={12}>
              <ButtonGroup style={{ width: '100%' }}>
                <Button
                  sx={{ fontSize: { xs: '3.5vw', md: '1vw' } }}
                  onClick={onBtnUpdatetClick}
                  className="bg-success w-100 text-white"
                >
                  Create Product
                </Button>
                <Button
                  sx={{ fontSize: { xs: '3.5vw', md: '1vw' } }}
                  onClick={onBtnCancelClick}
                  className="bg-secondary w-100 text-white"
                >
                  Hủy Bỏ
                </Button>
              </ButtonGroup>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handelCloseAlert}>
        <Alert onClose={handelCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default ModalEdit
