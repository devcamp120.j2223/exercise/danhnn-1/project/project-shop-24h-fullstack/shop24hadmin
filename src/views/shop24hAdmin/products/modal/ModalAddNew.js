import {
  Grid,
  Alert,
  Box,
  Button,
  FormControl,
  Modal,
  Select,
  Snackbar,
  Typography,
  MenuItem,
  ButtonGroup,
} from '@mui/material'
import { useState, useEffect } from 'react'
import { CFormInput } from '@coreui/react'
function ModalAddNew({
  openModalAdd,
  setOpenModalAdd,
  handleClose,
  style,
  fetchAPI,
  setVarRefeshPage,
  varRefeshPage,
}) {
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [type, setType] = useState('NOT')
  const [imageUrl, setImageUrl] = useState('')
  const [buyPrice, setBuyPrice] = useState('')
  const [promotionPrice, setPromotionPrice] = useState('')
  const [amount, setAmount] = useState('')

  //Alert
  const [openAlert, setOpenAlert] = useState(false)
  const [statusModal, setStatusModal] = useState('error')
  const [noidungAlertValid, setNoidungAlertValid] = useState('')

  //Select Type
  const [productTypes, setProductTypes] = useState([])

  useEffect(() => {
    fetchAPI('http://localhost:8000/producttypes')
      .then((data) => {
        setProductTypes(data.data)
        console.log(data.data)
      })
      .catch((error) => {
        console.error(error.message)
      })
  }, [])

  //BTN ADD NEW
  const onBtnInsertClick = () => {
    if (valiDate()) {
      const body = {
        method: 'POST',
        body: JSON.stringify({
          name: name,
          description: description,
          type: type,
          imageUrl: imageUrl,
          buyPrice: new Number(buyPrice),
          promotionPrice: new Number(promotionPrice),
          amount: new Number(amount),
        }),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      }
      fetchAPI('http://localhost:8000/products', body)
        .then((data) => {
          setOpenAlert(true)
          setStatusModal('success')
          setNoidungAlertValid('Dữ liệu thêm thành công!')
          setOpenModalAdd(false)
          setVarRefeshPage(varRefeshPage + 1)
          console.log(data)
        })
        .catch((error) => {
          setOpenAlert(true)
          setStatusModal('error')
          setNoidungAlertValid('Dữ liệu thêm thất bại!')
          console.log(error.message)
        })
    }
  }

  //Validate
  const valiDate = () => {
    if (name == '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa đặt tên cho sản phẩm')
      return false
    }
    if (description === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa có mô tả cho sản phẩm')
      return false
    }
    if (type === 'NOT') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa chọn loại sản phẩm')
      return false
    }
    if (imageUrl.indexOf('.') === -1) {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('ImageUrl không hợp lệ')
      return false
    }
    if (buyPrice === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa có giá sản phẩm')
      return false
    }
    if (promotionPrice === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa có giá mới sản phẩm')
      return false
    }
    if (amount === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa có số lượng sản phẩm')
      return false
    }
    return true
  }

  //Đóng Alert
  const handelCloseAlert = () => {
    setOpenAlert(false)
  }

  //Đóng Modal
  const onBtnCancelClick = () => {
    handleClose()
  }

  return (
    <>
      <Modal open={openModalAdd} onClose={handleClose}>
        <Box p={2} sx={style}>
          <Typography sx={{ fontSize: { xs: '4vw', md: '2vw' } }} align="center">
            <strong>Thêm Sản Phẩm</strong>
          </Typography>

          <Grid container pt={2}>
            <Grid container mt={1}>
              <Grid container>
                <CFormInput
                  fullWidth
                  placeholder="Name"
                  className="bg-white"
                  size="small"
                  value={name}
                  onChange={(event) => setName(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  value={description}
                  placeholder="Description"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setDescription(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <FormControl fullWidth>
                  <Select
                    value={type}
                    label="Age"
                    onChange={(event) => setType(event.target.value)}
                  >
                    <MenuItem value={'NOT'}>Type</MenuItem>
                    {productTypes.map((type, index) => {
                      return (
                        <MenuItem key={index} value={type._id}>
                          {type.name}
                        </MenuItem>
                      )
                    })}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  value={imageUrl}
                  placeholder="ImageUrl"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setImageUrl(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  type="number"
                  fullWidth
                  value={buyPrice}
                  placeholder="BuyPrice"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setBuyPrice(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  type="number"
                  fullWidth
                  value={promotionPrice}
                  placeholder="PromotionPrice"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setPromotionPrice(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  type="number"
                  fullWidth
                  value={amount}
                  placeholder="Amount"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setAmount(event.target.value)}
                />
              </Grid>
            </Grid>
          </Grid>

          <Grid className="mt-3 text-center">
            <Grid justifyContent={'center'} item sm={12}>
              <ButtonGroup style={{ width: '100%' }}>
                <Button
                  onClick={onBtnInsertClick}
                  sx={{ fontSize: { xs: '3.5vw', md: '1vw' } }}
                  className="bg-success w-100 text-white"
                >
                  Create Product
                </Button>
                <Button
                  sx={{ fontSize: { xs: '3.5vw', md: '1vw' } }}
                  onClick={onBtnCancelClick}
                  className="bg-secondary w-100 text-white"
                >
                  Hủy Bỏ
                </Button>
              </ButtonGroup>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handelCloseAlert}>
        <Alert onClose={handelCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default ModalAddNew
