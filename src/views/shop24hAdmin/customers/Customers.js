import React, { useEffect, useState } from 'react'

import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CButton,
} from '@coreui/react'

import {
  Grid,
  Pagination,
  InputLabel,
  Select,
  MenuItem,
  Button,
  ButtonGroup,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from '@mui/material'

import ModalAddNew from './modal/ModalAddNew'
import ModalDelete from './modal/ModalDelete'
import ModalEdit from './modal/ModalEdit'

//Style
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 360,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
  fontWeight: 'bold',
}

const Customers = () => {
  const [customers, setCustomers] = useState([])
  const [customerPhone, setCustomerPhone] = useState('')
  const [orders, setOrders] = useState([])

  //PANIGATION
  //Limit: số lượng bản ghi trên 1 trang
  const [limit, setLimit] = useState(10)
  //số trang: tổng số lượng sản phẩm / limit - Số lớn hơn gần nhất
  const [noPage, setNoPage] = useState(0)
  //Trang hiện tại
  const [page, setPage] = useState(1)
  //Load trang
  const [varRefeshPage, setVarRefeshPage] = useState(0)

  //PANIGATION
  const handleChangeLimit = (event) => {
    setLimit(event.target.value)
  }
  const onChangePagination = (event, value) => {
    setPage(value)
  }

  //MODAL
  const [openModalAdd, setOpenModalAdd] = useState(false)
  const [openModalEdit, setOpenModalEdit] = useState(false)
  const [openModalDelete, setOpenModalDelete] = useState(false)

  //Đóng Modal
  const handleClose = () => setOpenModalAdd(false)
  const handleCloseEdit = () => setOpenModalEdit(false)
  const handleCloseDelete = () => setOpenModalDelete(false)

  //Modal Add New
  const onBtnAddOrderClick = () => {
    console.log('Nút thêm được click')
    setOpenModalAdd(true)
  }

  //Modal Delete
  //ID
  const [idDelete, setIdDelete] = useState({ id: 0 })
  const [nameDelete, setNameDelete] = useState('')

  const onBtnDeleteClick = (data) => {
    console.log('Nút xóa được click')
    console.log('ID: ' + data._id)
    setOpenModalDelete(true)
    setIdDelete(data._id)
    setNameDelete(data.fullName)
  }
  const [dataEdit, setDataEdit] = useState({})
  const onBtnEditClick = (data) => {
    setDataEdit(data)
    setOpenModalEdit(true)
  }

  //LOAD  API
  const fetchAPI = async (url, body) => {
    const response = await fetch(url, body)
    const data = await response.json()
    return data
  }

  useEffect(() => {
    if (customerPhone == '') {
      fetchAPI('http://localhost:8000/customers')
        .then((data) => {
          setNoPage(Math.ceil(data.data.length / limit))

          setCustomers(data.data.slice((page - 1) * limit, page * limit))
          console.log(data)
        })
        .catch((error) => {
          console.log(error.message)
        })
    } else {
      fetchAPI(`http://localhost:8000/customers?phone=${customerPhone}`)
        .then((data) => {
          setNoPage(Math.ceil(data.data.length / limit))

          setCustomers(data.data.slice((page - 1) * limit, page * limit))
          console.log(data)
        })
        .catch((error) => {
          console.error(error.message)
        })
    }
  }, [customerPhone, page, limit, varRefeshPage])

  useEffect(() => {
    fetchAPI('http://localhost:8000/orders')
      .then((data) => {
        setOrders(data.data)
        console.log(data)
      })
      .catch((error) => {
        console.error(error.message)
      })
  }, [])

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Customers</strong>
          </CCardHeader>
          <CCardBody>
            <h3 className="text-medium-emphasis" align="center">
              Danh sách khách hàng
            </h3>

            <Grid container mt={1} mb={1}>
              <Grid item xs="4">
                <Grid item xs={12}>
                  <CButton onClick={onBtnAddOrderClick} className="btn btn-success text-white">
                    Add new
                  </CButton>
                </Grid>
              </Grid>
              <Grid item xs="8">
                <Grid container justifyContent={'flex-end'}>
                  <Grid item marginY={'auto'} mr={1}>
                    <InputLabel>Show products</InputLabel>
                  </Grid>
                  <Grid item>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={limit}
                      size="small"
                      onChange={handleChangeLimit}
                    >
                      <MenuItem value={5}>5</MenuItem>
                      <MenuItem value={10}>10</MenuItem>
                      <MenuItem value={25}>25</MenuItem>
                      <MenuItem value={50}>50</MenuItem>
                    </Select>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>

            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell align="center">Fullname</TableCell>
                    <TableCell align="center">Phone</TableCell>
                    <TableCell align="center">Email</TableCell>
                    <TableCell align="center">Address</TableCell>
                    <TableCell align="center">Action</TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {customers.map((customer, index) => {
                    return (
                      <TableRow key={index}>
                        <TableCell>{customer.fullName}</TableCell>
                        <TableCell>{customer.phone}</TableCell>
                        <TableCell>{customer.email}</TableCell>
                        <TableCell>{customer.address}</TableCell>
                        <TableCell>
                          <Button
                            onClick={() => {
                              onBtnEditClick(customer)
                            }}
                            variant="contained"
                            className="bg-success w-50"
                          >
                            EDIT
                          </Button>
                          <Button
                            onClick={() => {
                              onBtnDeleteClick(customer)
                            }}
                            variant="contained"
                            color="error"
                            className=" w-50"
                          >
                            DELETE
                          </Button>
                        </TableCell>
                      </TableRow>
                    )
                  })}
                </TableBody>
              </Table>
            </TableContainer>

            <Grid container mt={3} mb={2} justifyContent="flex-end">
              <Grid item>
                <Pagination
                  count={noPage}
                  color="primary"
                  defaultPage={1}
                  onChange={onChangePagination}
                />
              </Grid>
            </Grid>
          </CCardBody>
          <ModalAddNew
            varRefeshPage={varRefeshPage}
            setOpenModalAdd={setOpenModalAdd}
            openModalAdd={openModalAdd}
            handleClose={handleClose}
            style={style}
            fetchAPI={fetchAPI}
            setVarRefeshPage={setVarRefeshPage}
          />
          <ModalDelete
            varRefeshPage={varRefeshPage}
            setVarRefeshPage={setVarRefeshPage}
            style={style}
            openModalDelete={openModalDelete}
            idDelete={idDelete}
            nameDelete={nameDelete}
            handleCloseDelete={handleCloseDelete}
          />
          <ModalEdit
            varRefeshPage={varRefeshPage}
            setOpenModalEdit={setOpenModalEdit}
            openModalEdit={openModalEdit}
            handleCloseEdit={handleCloseEdit}
            style={style}
            fetchAPI={fetchAPI}
            setVarRefeshPage={setVarRefeshPage}
            dataEdit={dataEdit}
            setDataEdit={setDataEdit}
          />
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Customers
