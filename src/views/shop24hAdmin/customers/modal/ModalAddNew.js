import {
  Container,
  Grid,
  Alert,
  Box,
  Button,
  Modal,
  Snackbar,
  Typography,
  ButtonGroup,
} from '@mui/material'
import { useState, useEffect } from 'react'
import { CFormInput } from '@coreui/react'
function ModalAddNew({
  openModalAdd,
  setOpenModalAdd,
  handleClose,
  style,
  fetchAPI,
  setVarRefeshPage,
  varRefeshPage,
}) {
  const [fullName, setFullName] = useState('')
  const [phone, setphone] = useState('')
  const [email, setEmail] = useState('')
  const [address, setAddress] = useState('')
  const [city, setCity] = useState('')
  const [country, setCountry] = useState('')

  //Alert
  const [openAlert, setOpenAlert] = useState(false)
  const [statusModal, setStatusModal] = useState('error')
  const [noidungAlertValid, setNoidungAlertValid] = useState('')

  //BTN ADD NEW
  const onBtnInsertClick = () => {
    if (valiDate()) {
      const body = {
        method: 'POST',
        body: JSON.stringify({
          fullName: fullName,
          phone: phone,
          email: email,
          address: address,
          city: city,
          country: country,
        }),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      }
      fetchAPI('http://localhost:8000/customers/', body)
        .then((data) => {
          setOpenAlert(true)
          setStatusModal('success')
          setNoidungAlertValid('Dữ liệu thêm thành công!')
          setOpenModalAdd(false)
          setVarRefeshPage(varRefeshPage + 1)
          console.log(data)
          // window.location.reload();
        })
        .catch((error) => {
          setOpenAlert(true)
          setStatusModal('error')
          setNoidungAlertValid('Dữ liệu thêm thất bại!')
          console.log(error.message)
        })
    }
  }

  //Validate
  const valiDate = () => {
    if (fullName == '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa nhập tên khách hàng')
      return false
    }
    if (phone === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa nhập số điện thoại')
      return false
    }

    const vREG =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (!vREG.test(String(email))) {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Email khách hàng không hợp lệ')
      return false
    }

    if (address === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa nhập địa chỉ')
      return false
    }
    return true
  }

  //Đóng Alert
  const handelCloseAlert = () => {
    setOpenAlert(false)
  }

  //Đóng Modal
  const onBtnCancelClick = () => {
    handleClose()
  }

  return (
    <>
      <Modal open={openModalAdd} onClose={handleClose}>
        <Box p={2} sx={style}>
          <Typography sx={{ fontSize: { xs: '4vw', md: '2vw' } }} align="center">
            <strong>Thêm Khách Hàng</strong>
          </Typography>
          <Grid container pt={2}>
            <Grid container mt={1}>
              <Grid container>
                <CFormInput
                  fullWidth
                  placeholder="Full Name"
                  className="bg-white"
                  size="small"
                  value={fullName}
                  onChange={(event) => setFullName(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <Grid container>
                  <CFormInput
                    fullWidth
                    value={phone}
                    placeholder="phone"
                    className="bg-white"
                    size="small"
                    onChange={(event) => setphone(event.target.value)}
                  />
                </Grid>
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  value={email}
                  placeholder="Email"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setEmail(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  value={address}
                  placeholder="Address"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setAddress(event.target.value)}
                />
              </Grid>
            </Grid>
          </Grid>

          <Grid className="mt-3 text-center">
            <Grid justifyContent={'center'} item sm={12}>
              <ButtonGroup style={{ width: '100%' }}>
                <Button
                  onClick={onBtnInsertClick}
                  sx={{ fontSize: { xs: '3.5vw', md: '1vw' } }}
                  className="bg-success w-100 text-white"
                >
                  Create Customer
                </Button>
                <Button
                  sx={{ fontSize: { xs: '3.5vw', md: '1vw' } }}
                  onClick={onBtnCancelClick}
                  className="bg-secondary w-100 text-white"
                >
                  Hủy Bỏ
                </Button>
              </ButtonGroup>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handelCloseAlert}>
        <Alert onClose={handelCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default ModalAddNew
