import {
  Container,
  Grid,
  Alert,
  Box,
  Button,
  Modal,
  Snackbar,
  Typography,
  ButtonGroup,
} from '@mui/material'
import { useState, useEffect } from 'react'
import { CFormInput } from '@coreui/react'
function ModalEdit({
  varRefeshPage,
  setOpenModalEdit,
  openModalEdit,
  handleCloseEdit,
  style,
  fetchAPI,
  setVarRefeshPage,
  dataEdit,
  setDataEdit,
}) {
  //Alert
  const [openAlert, setOpenAlert] = useState(false)
  const [statusModal, setStatusModal] = useState('error')
  const [noidungAlertValid, setNoidungAlertValid] = useState('')

  //btn update customer
  const onBtnUpdateClick = () => {
    if (valiDate()) {
      const body = {
        method: 'PUT',
        body: JSON.stringify({
          fullName: dataEdit.fullName,
          phone: dataEdit.phone,
          email: dataEdit.email,
          address: dataEdit.address,
          city: dataEdit.city,
          country: dataEdit.country,
        }),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      }
      fetchAPI(`http://localhost:8000/customers/${dataEdit._id}`, body)
        .then((data) => {
          setOpenAlert(true)
          setStatusModal('success')
          setNoidungAlertValid('Dữ liệu thêm thành công!')
          setOpenModalEdit(false)
          setVarRefeshPage(varRefeshPage + 1)
          console.log(data)
        })
        .catch((error) => {
          setOpenAlert(true)
          setStatusModal('error')
          setNoidungAlertValid('Dữ liệu thêm thất bại!')
          console.log(error.message)
        })
    }
  }
  //Validate
  const valiDate = () => {
    if (dataEdit.fullName == '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa nhập tên khách hàng')
      return false
    }
    if (dataEdit.phone === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa nhập số điện thoại')
      return false
    }

    const vREG =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (!vREG.test(String(dataEdit.email))) {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Email khách hàng không hợp lệ')
      return false
    }

    if (dataEdit.address === '') {
      setOpenAlert(true)
      setStatusModal('error')
      setNoidungAlertValid('Chưa nhập địa chỉ')
      return false
    }
    return true
  }
  //Đóng Alert
  const handelCloseAlert = () => {
    setOpenAlert(false)
  }
  const setFullName = (data) => {
    setDataEdit({ ...dataEdit, fullName: data })
  }
  const setphone = (data) => {
    setDataEdit({ ...dataEdit, phone: data })
  }
  const setEmail = (data) => {
    setDataEdit({ ...dataEdit, email: data })
  }
  const setAddress = (data) => {
    setDataEdit({ ...dataEdit, address: data })
  }

  //Đóng Modal
  const onBtnCancelClick = () => {
    handleCloseEdit()
  }

  return (
    <>
      <Modal open={openModalEdit} onClose={handleCloseEdit}>
        <Box p={2} sx={style}>
          <Typography sx={{ fontSize: { xs: '4vw', md: '2vw' } }} align="center">
            <strong>Sửa Thông Tin Khách Hàng</strong>
          </Typography>
          <Grid container pt={2}>
            <Grid container mt={1}>
              <Grid container>
                <CFormInput
                  fullWidth
                  placeholder="Full Name"
                  className="bg-white"
                  size="small"
                  value={dataEdit.fullName}
                  onChange={(event) => setFullName(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <Grid container>
                  <CFormInput
                    fullWidth
                    value={dataEdit.phone}
                    placeholder="phone"
                    className="bg-white"
                    size="small"
                    onChange={(event) => setphone(event.target.value)}
                  />
                </Grid>
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  value={dataEdit.email}
                  placeholder="Email"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setEmail(event.target.value)}
                />
              </Grid>
            </Grid>

            <Grid container mt={2}>
              <Grid container>
                <CFormInput
                  fullWidth
                  value={dataEdit.address}
                  placeholder="Address"
                  className="bg-white"
                  size="small"
                  onChange={(event) => setAddress(event.target.value)}
                />
              </Grid>
            </Grid>
          </Grid>

          <Grid className="mt-3 text-center">
            <Grid justifyContent={'center'} item sm={12}>
              <ButtonGroup style={{ width: '100%' }}>
                <Button
                  onClick={onBtnUpdateClick}
                  sx={{ fontSize: { xs: '3.5vw', md: '1vw' } }}
                  className="bg-success w-100 text-white"
                >
                  Xác Nhận
                </Button>
                <Button
                  sx={{ fontSize: { xs: '3.5vw', md: '1vw' } }}
                  onClick={onBtnCancelClick}
                  className="bg-secondary w-100 text-white"
                >
                  Hủy Bỏ
                </Button>
              </ButtonGroup>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handelCloseAlert}>
        <Alert onClose={handelCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  )
}
export default ModalEdit
